# # # # # # # # # # #
#Initial settings----
# # # # # # # # # # #
if (!require(ggplot2)) {install.packages('ggplot2')}
library(ggplot2)
if (!require(MBESS)) {install.packages('MBESS')}
library(MBESS)
if (!require(pwr)) {install.packages('pwr')}
library(pwr)
if (!require(meta)) {install.packages('meta')}
library(meta)
if (!require(metafor)) {install.packages('metafor')}
library(metafor)
if (!require(Rcpp)) {install.packages('Rcpp')}
library(Rcpp)
if (!require(MASS)) {install.packages('MASS')}
library(MASS)
options(digits = 10, scipen = 999)
#Set color palette
cbbPalette <- c("#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

# # # # # # # # # #
#Assignment 2----
# # # # # # # # # #

nSims <- 100000 #number of simulated experiments
M<-106 #Mean IQ score in the sample (will be compared with 100 in a one-sample t-test)
n<-26 #set sample size
SD<-15 #SD of the simulated data
p <-numeric(nSims) #set up empty container for all simulated p-values
bars<-20 #set number of bars in plot

for(i in 1:nSims){ #for each simulated experiment
  x<-rnorm(n = n, mean = M, sd = SD) #
  z<-t.test(x, mu=100) #perform the t-test against mu (set to value you want to test against)
  p[i]<-z$p.value #get the p-value and store it
}
(sum(p < 0.05)/nSims)*100 #power (in percentage)

#Perform power analysis for one sample t-test
#The result using the closed formula (below) will differ slightly from the simulation - increase the number of simulations for a more accurate result
pwr.t.test(d = (M-100)/SD, 
           n = n,sig.level=0.05,
           type = "one.sample",
           alternative = "two.sided")$power #determines M when power > 0. When power = 0, will set  M = 100.

#Plot figure
op <- par(mar = c(5,7,4,4)) #change white-space around graph
hist(p, breaks=bars, xlab="P-values", ylab="number of p-values\n", axes=FALSE,
     main=paste("P-value Distribution with",round(power*100, digits=1),"% Power"),
     col="grey", xlim=c(0,1),  ylim=c(0, nSims))
axis(side=1, at=seq(0,1, 0.1), labels=seq(0,1,0.1))
axis(side=2, at=seq(0,nSims, nSims/4), labels=seq(0,nSims, nSims/4), las=2)
abline(h=nSims/bars, col = "red", lty=3) #draw a red line expected if the null is true


#power analysis for correlation
n <- 50
cor.true <- 0.3
mx<-106  #set the mean in group 1
sdx<-15  #set the standard deviation in group 1
my<-100  #set the mean in group 2
sdy<-15  #set the standard deviation in group 2
nSims<-100000
#This formula creates a second sample with identical mean and sd, which is correlated with the first sample.
p <-numeric(nSims) #set up empty container for all simulated p-values
for(i in 1:nSims){ #for each simulated experiment
  #randomly create correlated data using the mvrnorm function in the MASS package
  cov.mat <- matrix(c(1.0, cor.true, cor.true, 1.0), nrow = 2, byrow = T)
  mu <- c(0,0)
  mat <- mvrnorm(n, Sigma = cov.mat, mu = mu, empirical = FALSE)
  x<-mat[,1]*sdx+mx
  y<-mat[,2]*sdy+my
  z<-cor.test(x,y) #perform the correlation test
  p[i]<-z$p.value #get the p-value and store it
}
(sum(p < 0.05)/nSims)*100 #power (in percentage)

#Plot figure
op <- par(mar = c(5,7,4,4)) #change white-space around graph
hist(p, breaks=bars, xlab="P-values", ylab="number of p-values\n", axes=FALSE,
     main=paste("P-value Distribution"),
     col="grey", xlim=c(0,1),  ylim=c(0, nSims))
axis(side=1, at=seq(0,1, 0.1), labels=seq(0,1,0.1))
axis(side=2, at=seq(0,nSims, nSims/4), labels=seq(0,nSims, nSims/4), las=2)

#Perform power analysis 
pwr.r.test(r=0.3,n=50,sig.level=0.05,alternative="two.sided") 



#Install metafor and MBESS packages
if(!require(metafor)){install.packages('metafor')}
library(metafor)
if(!require(MBESS)){install.packages('MBESS')}
library(MBESS)

#Example of an effect size calculation-------------------------
g <- escalc(n1i = 50, 
            n2i = 53, 
            m1i = 5.6, 
            m2i = 4.9, 
            sd1i = 1.2, 
            sd2i = 1.3, 
            measure = "SMD")
#show the effect size, which is printed under yi, and variance, under vi
g
#      yi     vi
#1 0.5547 0.0404

#Now generate multiple studies to simulate meta-analyses----------------

#number of simulated experiments
nSims <- 12

#set means and sd's for the true population we will sample from
pop.m1 <- 106
pop.sd1 <- 15
pop.m2 <- 100
pop.sd2 <- 15
metadata <- data.frame(yi= numeric(0), 
                       vi = numeric(0))

#What is our effect size? 
(pop.m1 - pop.m2) / (pop.sd1 + pop.sd2) / 2

for(i in 1:nSims){ #for each simulated experiment
  n <- sample(30 : 100, 1) #draw a sample size of x to y
  x <- rnorm(n = n, 
             mean = pop.m1, 
             sd = pop.sd1) #produce  simulated participants
  y <- rnorm(n = n, 
             mean = pop.m2, 
             sd = pop.sd2) #produce  simulated participants
  metadata[i,1] <- escalc(n1i = n, 
                          n2i = n, 
                          m1i = mean(x), 
                          m2i = mean(y), 
                          sd1i = sd(x), 
                          sd2i = sd(y), 
                          measure = "SMD")$yi
  metadata[i,2] <- escalc(n1i = n, 
                          n2i = n, 
                          m1i = mean(x), 
                          m2i = mean(y), 
                          sd1i = sd(x), 
                          sd2i = sd(y), 
                          measure = "SMD")$vi
}

#Use the metafor package to perform a meta-analysis
result <- rma(yi, vi, data = metadata)
#Give the results
result 
#plot a default forest plot
forest(result)